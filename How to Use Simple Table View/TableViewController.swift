//
//  TableViewController.swift
//  How to Use Simple Table View
//
//  Created by Mahesh Prasad on 17/08/18.
//  Copyright © 2018 Mahesh Prasad. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    let data = ["First","Second","Third","Fourth","Five","Six"]
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
            cell?.textLabel?.text = data[indexPath.row]
        return cell!
    }
}
